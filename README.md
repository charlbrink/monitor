# Spring Cloud Monitoring#

The application obtains its configuration from the spring cloud configuration server (localhost:8001), uses the Eureka registry to find applications to monitor.

Monitoring dashboard using Hystrix and Turbine at:
<pre>http://localhost:8989/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8989%2Fturbine.stream</pre>